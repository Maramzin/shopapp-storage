package ru.maramzin.shopapp.storage.business.facade;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Pair;
import ru.maramzin.shopapp.storage.business.service.ProductInfoService;
import ru.maramzin.shopapp.storage.business.service.history.DiscountHistoryService;
import ru.maramzin.shopapp.storage.business.service.history.PriceHistoryService;
import ru.maramzin.shopapp.storage.business.service.history.ReplenishmentHistoryService;
import ru.maramzin.shopapp.storage.business.service.history.SellingHistoryService;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseResponse;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;

@ExtendWith(MockitoExtension.class)
class ProductFacadeTest {

  @Mock
  private ProductInfoService productInfoService;

  @Mock
  private SellingHistoryService sellingHistoryService;

  @Mock
  private ReplenishmentHistoryService replenishmentHistoryService;

  @Mock
  private PriceHistoryService priceHistoryService;

  @Mock
  private DiscountHistoryService discountHistoryService;

  @InjectMocks
  private ProductFacade productFacade;

  private ProductInfoResponse productInfoResponse;
  private SellingHistoryResponse sellingHistoryResponse;
  private ReplenishmentHistoryResponse replenishmentHistoryResponse;
  private PriceHistoryResponse priceHistoryResponse;
  private DiscountHistoryResponse discountHistoryResponse;

  @BeforeEach
  void setUp() {
    UUID productId = UUID.randomUUID();
    UUID sellerId = UUID.randomUUID();
    productInfoResponse = ProductInfoResponse.builder()
        .id(productId)
        .name("test")
        .description("test")
        .cost(9999)
        .discount(15)
        .quantity(100)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    discountHistoryResponse = DiscountHistoryResponse.builder()
        .productInfoId(productId)
        .datetime(LocalDateTime.now())
        .discountBefore(10)
        .discountAfter(15)
        .userId(sellerId)
        .build();

    priceHistoryResponse = PriceHistoryResponse.builder()
        .productInfoId(productId)
        .datetime(LocalDateTime.now())
        .costBefore(10000)
        .costAfter(9999)
        .userId(sellerId)
        .build();

    replenishmentHistoryResponse = ReplenishmentHistoryResponse.builder()
        .productInfoId(productId)
        .datetime(LocalDateTime.now())
        .quantity(10)
        .userId(sellerId)
        .build();

    sellingHistoryResponse = SellingHistoryResponse.builder()
        .productInfoId(productId)
        .datetime(LocalDateTime.now())
        .quantity(10)
        .userId(sellerId)
        .build();
  }

  @Test
  void findSellingHistoriesByProductName_shouldWorkCorrectly() {
    String productName = "test";
    List<SellingHistoryResponse> expected = List.of(sellingHistoryResponse);
    Mockito.when(productInfoService.findByName(ArgumentMatchers.any())).thenReturn(productInfoResponse);
    Mockito.when(sellingHistoryService.findByProductInfoId(ArgumentMatchers.any()))
        .thenReturn(expected);

    List<SellingHistoryResponse> actual = productFacade.findSellingHistoriesByProductName(productName);

    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  void findReplenishmentHistoriesByProductName_shouldWorkCorrectly() {
    String productName = "test";
    List<ReplenishmentHistoryResponse> expected = List.of(replenishmentHistoryResponse);
    Mockito.when(productInfoService.findByName(ArgumentMatchers.any())).thenReturn(productInfoResponse);
    Mockito.when(replenishmentHistoryService.findByProductInfoId(ArgumentMatchers.any()))
        .thenReturn(expected);

    List<ReplenishmentHistoryResponse> actual = productFacade.findReplenishmentHistoriesByProductName(productName);

    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  void findPriceHistoriesByProductName_shouldWorkCorrectly() {
    String productName = "test";
    List<PriceHistoryResponse> expected = List.of(priceHistoryResponse);
    Mockito.when(productInfoService.findByName(ArgumentMatchers.any())).thenReturn(productInfoResponse);
    Mockito.when(priceHistoryService.findByProductInfoId(ArgumentMatchers.any()))
        .thenReturn(expected);

    List<PriceHistoryResponse> actual = productFacade.findPriceHistoriesByProductName(productName);

    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  void findDiscountHistoriesByProductName_shouldWorkCorrectly() {
    String productName = "test";
    List<DiscountHistoryResponse> expected = List.of(discountHistoryResponse);
    Mockito.when(productInfoService.findByName(ArgumentMatchers.any())).thenReturn(productInfoResponse);
    Mockito.when(discountHistoryService.findByProductInfoId(ArgumentMatchers.any()))
        .thenReturn(expected);

    List<DiscountHistoryResponse> actual = productFacade.findDiscountHistoriesByProductName(productName);

    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  void addProduct_shouldWorkCorrectly() {
    UUID sellerId = UUID.randomUUID();
    ProductInfoRequest request = ProductInfoRequest.builder()
        .name("test")
        .description("test")
        .cost(9999)
        .discount(15)
        .quantity(100)
        .clothesCategory(ClothesCategory.HAT)
        .seasonCategory(SeasonCategory.WINTER)
        .sellerId(sellerId)
        .build();

    Mockito.when(productInfoService.add(ArgumentMatchers.any())).thenReturn(productInfoResponse);
    Mockito.when(replenishmentHistoryService.add(ArgumentMatchers.any())).thenReturn(replenishmentHistoryResponse);

    ProductInfoResponse actual = productFacade.addProduct(request);
    Assertions.assertThat(actual).isEqualTo(productInfoResponse);
  }

  @Test
  void purchase_shouldWorkCorrectly() {
    Mockito.when(productInfoService.takeByName(ArgumentMatchers.any(), ArgumentMatchers.any()))
        .thenReturn(productInfoResponse);
    Mockito.when(sellingHistoryService.add(ArgumentMatchers.any()))
        .thenReturn(sellingHistoryResponse);

    PurchaseRequest request = PurchaseRequest.builder()
        .productName("test")
        .quantity(10)
        .build();

    List<PurchaseResponse> actual = productFacade.purchase(List.of(request), UUID.randomUUID());
    List<PurchaseResponse> expected = List.of(PurchaseResponse.builder()
        .productName(productInfoResponse.getName())
        .productId(productInfoResponse.getId())
        .cost(productInfoResponse.getCost())
        .discount(productInfoResponse.getDiscount())
        .quantity(request.getQuantity())
        .build());

    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  void replenishProduct_shouldWorkCorrectly() {
    String name = "test";
    Integer quantity = 1;
    UUID sellerId = UUID.randomUUID();
    Mockito.when(productInfoService.fillByName(ArgumentMatchers.any(),
        ArgumentMatchers.any(), ArgumentMatchers.any()))
        .thenReturn(productInfoResponse);
    Mockito.when(replenishmentHistoryService.add(ArgumentMatchers.any()))
        .thenReturn(replenishmentHistoryResponse);

    ProductInfoResponse actual = productFacade.replenishProduct(name, quantity, sellerId);
    Assertions.assertThat(actual).isEqualTo(productInfoResponse);
  }

  @Test
  void changePrice_shouldWorkCorrectly() {
    String name = "test";
    Integer newPrice = 10000;
    UUID sellerId = UUID.randomUUID();
    Mockito.when(productInfoService.changePriceByName(ArgumentMatchers.any(),
            ArgumentMatchers.any(), ArgumentMatchers.any()))
        .thenReturn(Pair.of(productInfoResponse, productInfoResponse.getCost()));
    Mockito.when(priceHistoryService.add(ArgumentMatchers.any()))
        .thenReturn(priceHistoryResponse);

    ProductInfoResponse actual = productFacade.changePrice(name, newPrice, sellerId);
    Assertions.assertThat(actual).isEqualTo(productInfoResponse);
  }

  @Test
  void changeDiscount_shouldWorkCorrectly() {
    String name = "test";
    Integer newDiscount = 10;
    UUID sellerId = UUID.randomUUID();
    Mockito.when(productInfoService.changeDiscountByName(ArgumentMatchers.any(),
            ArgumentMatchers.any(), ArgumentMatchers.any()))
        .thenReturn(Pair.of(productInfoResponse, productInfoResponse.getDiscount()));
    Mockito.when(discountHistoryService.add(ArgumentMatchers.any()))
        .thenReturn(discountHistoryResponse);

    ProductInfoResponse actual = productFacade.changeDiscount(name, newDiscount, sellerId);
    Assertions.assertThat(actual).isEqualTo(productInfoResponse);
  }
}