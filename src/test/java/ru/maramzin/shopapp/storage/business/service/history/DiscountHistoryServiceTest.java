package ru.maramzin.shopapp.storage.business.service.history;

import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.DiscountHistory;
import ru.maramzin.shopapp.storage.data.repository.DiscountHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.DiscountHistoryMapper;

@ExtendWith(MockitoExtension.class)
class DiscountHistoryServiceTest {
  @Mock
  private DiscountHistoryRepository repository;

  @Mock
  private DiscountHistoryMapper mapper;

  @InjectMocks
  private DiscountHistoryService service;

  @Test
  public void add_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    DiscountHistoryRequest request = DiscountHistoryRequest.builder()
        .productInfoId(productInfoId)
        .build();
    DiscountHistory history = DiscountHistory.builder()
        .productInfoId(productInfoId)
        .build();
    DiscountHistoryResponse expected = DiscountHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapRequestToEntity(request)).thenReturn(history);
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(expected);
    Mockito.when(repository.save(history)).thenReturn(history);

    DiscountHistoryResponse actual = service.add(request);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual.getProductInfoId()).isEqualTo(productInfoId);
  }

  @Test
  public void findByProductInfoId_shouldWorkCorrectly() {
    UUID productInfoId = UUID.randomUUID();
    DiscountHistory history = DiscountHistory.builder()
        .productInfoId(productInfoId)
        .build();
    List<DiscountHistory> histories = List.of(history);
    Mockito.when(repository.findAllByProductInfoId(productInfoId)).thenReturn(histories);

    DiscountHistoryResponse response = DiscountHistoryResponse.builder()
        .productInfoId(productInfoId)
        .build();
    Mockito.when(mapper.mapEntityToResponse(history)).thenReturn(response);
    List<DiscountHistoryResponse> expected = List.of(response);
    List<DiscountHistoryResponse> actual = service.findByProductInfoId(productInfoId);

    Assertions.assertThat(actual).isEqualTo(expected);
    Assertions.assertThat(actual).hasSize(1);
  }
}