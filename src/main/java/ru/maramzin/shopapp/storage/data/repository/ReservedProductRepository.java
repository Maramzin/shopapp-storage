package ru.maramzin.shopapp.storage.data.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.storage.data.entity.ReservedProduct;

/**
 * The interface Reserved product repository.
 */
public interface ReservedProductRepository extends JpaRepository<ReservedProduct, UUID> {

}
