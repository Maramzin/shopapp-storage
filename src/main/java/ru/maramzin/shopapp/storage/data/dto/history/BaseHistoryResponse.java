package ru.maramzin.shopapp.storage.data.dto.history;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * The type Base history response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class BaseHistoryResponse extends BaseHistoryDto {

  private UUID productInfoId;
  private LocalDateTime datetime;
}
