package ru.maramzin.shopapp.storage.data.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.storage.data.entity.SellingHistory;

/**
 * The interface Selling history repository.
 */
public interface SellingHistoryRepository extends JpaRepository<SellingHistory, UUID> {

  List<SellingHistory> findAllByProductInfoId(UUID productInfoId);

  List<SellingHistory> findAllByUserId(UUID userId);
}
