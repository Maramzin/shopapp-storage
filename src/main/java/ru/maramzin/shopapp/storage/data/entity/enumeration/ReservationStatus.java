package ru.maramzin.shopapp.storage.data.entity.enumeration;

/**
 * The enum Reservation status.
 */
public enum ReservationStatus {
  /**
   * Opened reservation status.
   */
  OPENED,
  /**
   * Returned reservation status.
   */
  RETURNED,
  /**
   * Committed reservation status.
   */
  COMMITTED
}
