package ru.maramzin.shopapp.storage.data.dto.operation.purchase;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;

/**
 * The type Purchase request.
 */
@Data
@Builder
public class PurchaseRequest {

  @NotBlank
  private final String productName;

  @NotNull
  @Positive
  private final Integer quantity;
}
