package ru.maramzin.shopapp.storage.data.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.maramzin.shopapp.storage.data.entity.ProductInfo;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;

/**
 * The interface Product info repository.
 */
public interface ProductInfoRepository extends JpaRepository<ProductInfo, UUID> {

  Optional<ProductInfo> findByName(String name);

  @Query("select product from ProductInfo product where "
      + "(:name is null or product.name like %:name%)"
      + "and (:costFrom is null or product.cost >= :costFrom)"
      + "and (:costTo is null or product.cost <= :costTo)"
      + "and (:discountFrom is null or product.discount >= :discountFrom)"
      + "and (:discountTo is null or product.discount <= :discountTo)"
      + "and (:clothesCategory is null or product.clothesCategory like :clothesCategory)"
      + "and (:seasonCategory is null or product.seasonCategory like :seasonCategory)"
      + "and (cast(:sellerId as uuid) is null or product.sellerId = :sellerId)")
  List<ProductInfo> filter(
      @Param("name") String name,
      @Param("costFrom") Integer costFrom,
      @Param("costTo") Integer costTo,
      @Param("discountFrom") Integer discountFrom,
      @Param("discountTo") Integer discountTo,
      @Param("clothesCategory") ClothesCategory clothesCategory,
      @Param("seasonCategory") SeasonCategory seasonCategory,
      @Param("sellerId") UUID sellerId);
}
