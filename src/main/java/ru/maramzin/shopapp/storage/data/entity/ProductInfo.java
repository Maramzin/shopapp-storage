package ru.maramzin.shopapp.storage.data.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;

/**
 * The type Product info.
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "product_info")
@Entity
public class ProductInfo {

  @Id
  private UUID id;

  private String name;
  private String description;
  private Integer cost;
  private Integer discount;
  private Integer quantity;

  @Enumerated(EnumType.STRING)
  private ClothesCategory clothesCategory;

  @Enumerated(EnumType.STRING)
  private SeasonCategory seasonCategory;
  private UUID sellerId;
}
