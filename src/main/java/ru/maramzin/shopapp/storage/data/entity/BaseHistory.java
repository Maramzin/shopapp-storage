package ru.maramzin.shopapp.storage.data.entity;

import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;

/**
 * The type Base history.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@MappedSuperclass
public class BaseHistory {

  @Id
  private UUID id;
  private UUID productInfoId;

  @CreatedDate
  private LocalDateTime datetime;
  private UUID userId;
}
