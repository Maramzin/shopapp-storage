package ru.maramzin.shopapp.storage.utils.exception;

/**
 * The type Product info exception.
 */
public class ProductInfoException extends RuntimeException {

  /**
   * Instantiates a new Product info exception.
   *
   * @param message the message
   * @param cause   the cause
   */
  public ProductInfoException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new Product info exception.
   *
   * @param message the message
   */
  public ProductInfoException(String message) {
    super(message);
  }
}
