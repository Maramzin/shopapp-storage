package ru.maramzin.shopapp.storage.utils.exception;

/**
 * The type Not enough quantity exception.
 */
public class ValidationException extends RuntimeException {

  /**
   * Instantiates a new Not enough quantity exception.
   *
   * @param message the message
   * @param cause   the cause
   */
  public ValidationException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new Not enough quantity exception.
   *
   * @param message the message
   */
  public ValidationException(String message) {
    super(message);
  }
}
