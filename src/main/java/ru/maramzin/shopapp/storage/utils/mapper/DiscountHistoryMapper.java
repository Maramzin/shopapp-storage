package ru.maramzin.shopapp.storage.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.DiscountHistory;

/**
 * The type Discount history mapper.
 */
@RequiredArgsConstructor
@Component
public class DiscountHistoryMapper implements
    EntityMapper<DiscountHistory, DiscountHistoryRequest, DiscountHistoryResponse> {

  private final ObjectMapper mapper;

  @Override
  public DiscountHistory mapRequestToEntity(DiscountHistoryRequest request) {
    return mapper.convertValue(request, DiscountHistory.class);
  }

  @Override
  public DiscountHistoryRequest mapEntityToRequest(DiscountHistory entity) {
    return mapper.convertValue(entity, DiscountHistoryRequest.class);
  }

  @Override
  public DiscountHistory mapResponseToEntity(DiscountHistoryResponse response) {
    return mapper.convertValue(response, DiscountHistory.class);
  }

  @Override
  public DiscountHistoryResponse mapEntityToResponse(DiscountHistory entity) {
    return mapper.convertValue(entity, DiscountHistoryResponse.class);
  }
}
