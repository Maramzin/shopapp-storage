package ru.maramzin.shopapp.storage.utils.logs;

/**
 * The enum Exception messages.
 */
public enum ExceptionMessages {
  ;
  /**
   * The constant PRODUCT_INFO_NOT_FOUND_BY_NAME.
   */
  public static final String PRODUCT_INFO_NOT_FOUND_BY_NAME = "Product info not found by name '%s'";

  /**
   * The constant NOT_ENOUGH_QUANTITY.
   */
  public static final String NOT_ENOUGH_QUANTITY = "Product with name '%s' does not "
      + "contain enough quantity '%d'";

  public static final String SELLER_HAS_NO_PERMISSION = "Seller with id '%s' has "
      + "no permission for such action";

  public static final String RESERVATION_NOT_FOUND_BY_ID = "Reservation not found by id '%s'";
  public static final String RESERVATION_NOT_OPENED = "Reservation is not opened";
}
