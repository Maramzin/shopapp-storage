package ru.maramzin.shopapp.storage.business.service.history;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.SellingHistory;
import ru.maramzin.shopapp.storage.data.repository.SellingHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.SellingHistoryMapper;

/**
 * The type Selling history service.
 */
@RequiredArgsConstructor
@Qualifier("selling")
@Service
@Slf4j
public class SellingHistoryService implements
    HistoryService<SellingHistoryRequest, SellingHistoryResponse> {

  private final SellingHistoryRepository repository;

  private final SellingHistoryMapper mapper;

  @Transactional
  @Override
  public SellingHistoryResponse add(SellingHistoryRequest request) {
    SellingHistory history = mapper.mapRequestToEntity(request);
    history.setId(UUID.randomUUID());
    history.setDatetime(LocalDateTime.now());
    repository.save(history);

    log.info("Selling history has been saved to repository {}", history);
    return mapper.mapEntityToResponse(history);
  }

  @Transactional
  @Override
  public List<SellingHistoryResponse> findByProductInfoId(UUID productInfoId) {
    List<SellingHistory> histories = repository.findAllByProductInfoId(productInfoId);
    return histories.stream()
        .map(mapper::mapEntityToResponse)
        .toList();
  }
}
