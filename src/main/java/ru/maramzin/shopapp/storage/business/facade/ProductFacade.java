package ru.maramzin.shopapp.storage.business.facade;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.storage.business.service.ProductInfoService;
import ru.maramzin.shopapp.storage.business.service.ReservationService;
import ru.maramzin.shopapp.storage.business.service.history.DiscountHistoryService;
import ru.maramzin.shopapp.storage.business.service.history.PriceHistoryService;
import ru.maramzin.shopapp.storage.business.service.history.ReplenishmentHistoryService;
import ru.maramzin.shopapp.storage.business.service.history.SellingHistoryService;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.replenishment.ReplenishmentHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryResponse;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseResponse;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseResponseWrapper;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.ReservationResponse;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.reservation.product.ReservedProductRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ReservationStatus;

/**
 * The type Product facade.
 */
@RequiredArgsConstructor
@Service
public class ProductFacade {

  private final ProductInfoService productInfoService;
  private final SellingHistoryService sellingHistoryService;
  private final ReplenishmentHistoryService replenishmentHistoryService;
  private final PriceHistoryService priceHistoryService;
  private final DiscountHistoryService discountHistoryService;
  private final ReservationService reservationService;

  /**
   * Find selling histories by product name.
   *
   * @param productName the product name
   * @return the list
   */
  @Transactional
  public List<SellingHistoryResponse> findSellingHistoriesByProductName(String productName) {
    ProductInfoResponse productInfoResponse = productInfoService.findByName(productName);
    return sellingHistoryService.findByProductInfoId(productInfoResponse.getId());
  }

  /**
   * Find replenishment histories by product name.
   *
   * @param productName the product name
   * @return the list
   */
  @Transactional
  public List<ReplenishmentHistoryResponse> findReplenishmentHistoriesByProductName(
      String productName) {
    ProductInfoResponse productInfoResponse = productInfoService.findByName(productName);
    return replenishmentHistoryService.findByProductInfoId(productInfoResponse.getId());
  }

  /**
   * Find price histories by product name list.
   *
   * @param productName the product name
   * @return the list
   */
  @Transactional
  public List<PriceHistoryResponse> findPriceHistoriesByProductName(
      String productName) {
    ProductInfoResponse productInfoResponse = productInfoService.findByName(productName);
    return priceHistoryService.findByProductInfoId(productInfoResponse.getId());
  }

  /**
   * Find discount histories by product name list.
   *
   * @param productName the product name
   * @return the list
   */
  @Transactional
  public List<DiscountHistoryResponse> findDiscountHistoriesByProductName(
      String productName) {
    ProductInfoResponse productInfoResponse = productInfoService.findByName(productName);
    return discountHistoryService.findByProductInfoId(productInfoResponse.getId());
  }

  /**
   * Add product.
   *
   * @param request the request
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse addProduct(ProductInfoRequest request) {
    ProductInfoResponse response = productInfoService.add(request);
    ReplenishmentHistoryRequest replenishmentHistoryRequest = ReplenishmentHistoryRequest.builder()
        .productInfoId(response.getId())
        .userId(response.getSellerId())
        .quantity(response.getQuantity())
        .build();
    replenishmentHistoryService.add(replenishmentHistoryRequest);

    return response;
  }

  /**
   * Reserve purchase.
   *
   * @param purchaseRequests the purchase requests
   * @param clientId         the client id
   * @return the purchase response wrapper
   */
  @Transactional
  public PurchaseResponseWrapper reservePurchase(List<PurchaseRequest> purchaseRequests,
      UUID clientId) {
    UUID reservationId = reservationService.create(clientId).getId();
    List<PurchaseResponse> purchaseResponses = purchaseRequests.stream()
        .map((purchaseRequest -> reserveProduct(purchaseRequest, clientId, reservationId)))
        .toList();

    return PurchaseResponseWrapper.builder()
        .content(purchaseResponses)
        .reservationId(reservationId)
        .build();
  }

  /**
   * Rollback purchase.
   *
   * @param reservationId the reservation id
   */
  @Transactional
  public void rollbackPurchase(UUID reservationId) {
    ReservationResponse reservationResponse =
        reservationService.updateStatus(reservationId, ReservationStatus.RETURNED);

    reservationResponse.getReservedProducts().forEach(reserved -> productInfoService
        .returnToStorage(reserved.getProductInfo().getName(), reserved.getQuantity()));
  }

  /**
   * Commit purchase.
   *
   * @param reservationId the reservation id
   */
  @Transactional
  public void commitPurchase(UUID reservationId) {
    ReservationResponse reservationResponse =
        reservationService.updateStatus(reservationId, ReservationStatus.COMMITTED);

    reservationResponse.getReservedProducts().forEach(reserved -> {
      SellingHistoryRequest sellingHistoryRequest = SellingHistoryRequest.builder()
          .productInfoId(reserved.getProductInfo().getId())
          .userId(reservationResponse.getClientId())
          .quantity(reserved.getQuantity())
          .build();
      sellingHistoryService.add(sellingHistoryRequest);
    });
  }

  /**
   * Replenish product.
   *
   * @param productName the product name
   * @param quantity    the quantity
   * @param sellerId    the seller id
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse replenishProduct(String productName, Integer quantity, UUID sellerId) {
    ProductInfoResponse productResponse = productInfoService.fillByName(
        productName, quantity, sellerId);
    ReplenishmentHistoryRequest replenishmentHistoryRequest = ReplenishmentHistoryRequest.builder()
        .productInfoId(productResponse.getId())
        .userId(sellerId)
        .quantity(quantity)
        .build();
    replenishmentHistoryService.add(replenishmentHistoryRequest);

    return productResponse;
  }

  /**
   * Change price product info response.
   *
   * @param productName the product name
   * @param price       the new price
   * @param sellerId    the seller id
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse changePrice(String productName, Integer price, UUID sellerId) {
    Pair<ProductInfoResponse, Integer> responsePrevCostPair = productInfoService.changePriceByName(
        productName, price, sellerId);

    ProductInfoResponse response = responsePrevCostPair.getFirst();
    Integer prevCost = responsePrevCostPair.getSecond();
    priceHistoryService.add(PriceHistoryRequest.builder()
        .productInfoId(response.getId())
        .userId(sellerId)
        .costBefore(prevCost)
        .costAfter(price)
        .build());

    return response;
  }

  /**
   * Change discount product info response.
   *
   * @param productName the product name
   * @param discount    the new discount
   * @param sellerId    the seller id
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse changeDiscount(String productName, Integer discount,
      UUID sellerId) {
    Pair<ProductInfoResponse, Integer> responsePrevDiscountPair =
        productInfoService.changeDiscountByName(productName, discount, sellerId);

    ProductInfoResponse response = responsePrevDiscountPair.getFirst();
    Integer prevDiscount = responsePrevDiscountPair.getSecond();
    discountHistoryService.add(DiscountHistoryRequest.builder()
        .productInfoId(response.getId())
        .userId(sellerId)
        .discountBefore(prevDiscount)
        .discountAfter(discount)
        .build());

    return response;
  }

  private PurchaseResponse reserveProduct(PurchaseRequest purchaseRequest,
      UUID clientId, UUID reservationId) {
    String productName = purchaseRequest.getProductName();
    Integer quantity = purchaseRequest.getQuantity();

    ProductInfoResponse productResponse = productInfoService.takeByName(productName, quantity);
    reservationService.addReservedProduct(ReservedProductRequest.builder()
        .reservationId(reservationId)
        .productName(productName)
        .quantity(quantity)
        .build());



    return PurchaseResponse.builder()
        .productId(productResponse.getId())
        .productName(productName)
        .cost(productResponse.getCost())
        .discount(productResponse.getDiscount())
        .quantity(quantity)
        .build();
  }
}
