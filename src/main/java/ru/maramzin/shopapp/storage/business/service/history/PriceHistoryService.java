package ru.maramzin.shopapp.storage.business.service.history;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.price.PriceHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.PriceHistory;
import ru.maramzin.shopapp.storage.data.repository.PriceHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.PriceHistoryMapper;

/**
 * The type Price history service.
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class PriceHistoryService implements
    HistoryService<PriceHistoryRequest, PriceHistoryResponse> {

  private final PriceHistoryRepository repository;
  private final PriceHistoryMapper mapper;

  @Override
  public PriceHistoryResponse add(PriceHistoryRequest request) {
    PriceHistory history = mapper.mapRequestToEntity(request);
    history.setId(UUID.randomUUID());
    history.setDatetime(LocalDateTime.now());
    repository.save(history);

    log.info("Price history has been saved to repository {}", history);
    return mapper.mapEntityToResponse(history);
  }

  @Override
  public List<PriceHistoryResponse> findByProductInfoId(UUID productInfoId) {
    List<PriceHistory> histories = repository.findAllByProductInfoId(productInfoId);
    return histories.stream()
        .map(mapper::mapEntityToResponse)
        .toList();
  }
}
