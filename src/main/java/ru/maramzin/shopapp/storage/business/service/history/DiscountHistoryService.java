package ru.maramzin.shopapp.storage.business.service.history;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryRequest;
import ru.maramzin.shopapp.storage.data.dto.history.discount.DiscountHistoryResponse;
import ru.maramzin.shopapp.storage.data.entity.DiscountHistory;
import ru.maramzin.shopapp.storage.data.repository.DiscountHistoryRepository;
import ru.maramzin.shopapp.storage.utils.mapper.DiscountHistoryMapper;

/**
 * The type Discount history service.
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class DiscountHistoryService implements
    HistoryService<DiscountHistoryRequest, DiscountHistoryResponse> {

  private final DiscountHistoryRepository repository;
  private final DiscountHistoryMapper mapper;

  @Override
  public DiscountHistoryResponse add(DiscountHistoryRequest request) {
    DiscountHistory history = mapper.mapRequestToEntity(request);
    history.setId(UUID.randomUUID());
    history.setDatetime(LocalDateTime.now());
    repository.save(history);

    log.info("Discount history has been saved to repository {}", history);
    return mapper.mapEntityToResponse(history);
  }

  @Override
  public List<DiscountHistoryResponse> findByProductInfoId(UUID productInfoId) {
    List<DiscountHistory> histories = repository.findAllByProductInfoId(productInfoId);
    return histories.stream()
        .map(mapper::mapEntityToResponse)
        .toList();
  }
}
