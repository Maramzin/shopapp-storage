package ru.maramzin.shopapp.storage.business.service;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.storage.data.dto.operation.filter.FilterRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.ProductInfo;
import ru.maramzin.shopapp.storage.data.repository.ProductInfoRepository;
import ru.maramzin.shopapp.storage.utils.exception.ProductInfoException;
import ru.maramzin.shopapp.storage.utils.exception.ValidationException;
import ru.maramzin.shopapp.storage.utils.logs.ExceptionMessages;
import ru.maramzin.shopapp.storage.utils.mapper.ProductInfoMapper;

/**
 * The type Product info service.
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class ProductInfoService {

  private final ProductInfoMapper mapper;
  private final ProductInfoRepository repository;

  /**
   * Add product info response.
   *
   * @param request the request
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse add(ProductInfoRequest request) {
    ProductInfo productInfo = mapper.mapRequestToEntity(request);
    productInfo.setId(UUID.randomUUID());
    repository.save(productInfo);

    log.info("Product info has been saved to repository {}", productInfo);

    return mapper.mapEntityToResponse(productInfo);
  }

  /**
   * Find by name product info response.
   *
   * @param name the name
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse findByName(String name) {
    return mapper.mapEntityToResponse(findEntityByName(name));
  }

  /**
   * Delete by name product info response.
   *
   * @param name the name
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse deleteByName(String name) {
    ProductInfo productInfo = findEntityByName(name);
    repository.delete(productInfo);
    return mapper.mapEntityToResponse(productInfo);
  }

  /**
   * Take by name product info response.
   *
   * @param name     the name
   * @param quantity the quantity
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse takeByName(String name, Integer quantity) {
    ProductInfo productInfo = findEntityByName(name);

    int qtyDiff = productInfo.getQuantity() - quantity;
    if (qtyDiff < 0) {
      throw new ValidationException(String.format(
          ExceptionMessages.NOT_ENOUGH_QUANTITY, name, quantity));
    }

    productInfo.setQuantity(qtyDiff);
    repository.save(productInfo);

    log.info("Quantity has been reduced in product info {}", productInfo);
    return mapper.mapEntityToResponse(productInfo);
  }

  /**
   * Fill by name product info response.
   *
   * @param name     the name
   * @param quantity the quantity
   * @param sellerId the seller id
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse fillByName(String name, Integer quantity, UUID sellerId) {
    ProductInfo productInfo = findEntityByName(name);

    if (!productInfo.getSellerId().equals(sellerId)) {
      throw new ValidationException(String.format(
          ExceptionMessages.SELLER_HAS_NO_PERMISSION, sellerId));
    }

    int totalQty = productInfo.getQuantity() + quantity;
    productInfo.setQuantity(totalQty);
    repository.save(productInfo);

    log.info("Quantity has been replenished in product info {}", productInfo);
    return mapper.mapEntityToResponse(productInfo);
  }

  /**
   * Return to storage product info response.
   *
   * @param name     the name
   * @param quantity the quantity
   * @return the product info response
   */
  @Transactional
  public ProductInfoResponse returnToStorage(String name, Integer quantity) {
    ProductInfo productInfo = findEntityByName(name);

    int totalQty = productInfo.getQuantity() + quantity;
    productInfo.setQuantity(totalQty);
    repository.save(productInfo);

    log.info("Product {} has been returned to the storage: {}", name, quantity);
    return mapper.mapEntityToResponse(productInfo);
  }

  /**
   * Change price by name pair.
   *
   * @param name     the name
   * @param newPrice the new price
   * @param sellerId the seller id
   * @return the pair of product response and previous cost
   */
  @Transactional
  public Pair<ProductInfoResponse, Integer> changePriceByName(String name, Integer newPrice,
      UUID sellerId) {
    ProductInfo productInfo = findEntityByName(name);
    if (!productInfo.getSellerId().equals(sellerId)) {
      throw new ValidationException(String.format(
          ExceptionMessages.SELLER_HAS_NO_PERMISSION, sellerId));
    }

    final Integer prevCost = productInfo.getCost();
    productInfo.setCost(newPrice);
    repository.save(productInfo);

    log.info("Cost has been changed in product info {}", productInfo);
    ProductInfoResponse response = mapper.mapEntityToResponse(productInfo);
    return Pair.of(response, prevCost);
  }

  /**
   * Change discount by name pair.
   *
   * @param name        the name
   * @param newDiscount the new discount
   * @param sellerId    the seller id
   * @return the pair
   */
  @Transactional
  public Pair<ProductInfoResponse, Integer> changeDiscountByName(String name, Integer newDiscount,
      UUID sellerId) {
    ProductInfo productInfo = findEntityByName(name);
    if (!productInfo.getSellerId().equals(sellerId)) {
      throw new ValidationException(String.format(
          ExceptionMessages.SELLER_HAS_NO_PERMISSION, sellerId));
    }

    final Integer prevDiscount = productInfo.getDiscount();
    productInfo.setDiscount(newDiscount);
    repository.save(productInfo);

    log.info("Discount has been changed in product info {}", productInfo);
    ProductInfoResponse response = mapper.mapEntityToResponse(productInfo);
    return Pair.of(response, prevDiscount);
  }

  /**
   * Find all list.
   *
   * @return the list
   */
  @Transactional
  public List<ProductInfoResponse> findAll() {
    return repository.findAll()
        .stream()
        .map(mapper::mapEntityToResponse)
        .toList();
  }

  /**
   * Filter list.
   *
   * @param filterRequest the filter request
   * @return the list
   */
  @Transactional
  public List<ProductInfoResponse> filter(FilterRequest filterRequest) {
    List<ProductInfo> filteredProducts = repository.filter(
        filterRequest.getName(),
        filterRequest.getCostFrom(),
        filterRequest.getCostTo(),
        filterRequest.getDiscountFrom(),
        filterRequest.getDiscountTo(),
        filterRequest.getClothesCategory(),
        filterRequest.getSeasonCategory(),
        filterRequest.getSellerId());

    log.info("Product list has been found by filter: {}", filteredProducts);
    return filteredProducts.stream()
        .map(mapper::mapEntityToResponse)
        .toList();
  }

  private ProductInfo findEntityByName(String name) {

    ProductInfo productInfo = repository.findByName(name)
        .orElseThrow(() -> new ProductInfoException(String.format(
            ExceptionMessages.PRODUCT_INFO_NOT_FOUND_BY_NAME, name)));

    log.info("Product info has been extracted by name {}: {}", name, productInfo);
    return productInfo;
  }
}
