package ru.maramzin.shopapp.storage.ingress.controller;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.business.service.ProductInfoService;
import ru.maramzin.shopapp.storage.data.dto.operation.filter.FilterRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseRequest;
import ru.maramzin.shopapp.storage.data.dto.operation.purchase.PurchaseResponseWrapper;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoRequest;
import ru.maramzin.shopapp.storage.data.dto.product.ProductInfoResponse;
import ru.maramzin.shopapp.storage.data.entity.enumeration.ClothesCategory;
import ru.maramzin.shopapp.storage.data.entity.enumeration.SeasonCategory;
import ru.maramzin.shopapp.storage.utils.logs.FilterMapGenerator;

/**
 * The type Product controller.
 */
@Slf4j
@Validated
@RequestMapping("/product")
@RequiredArgsConstructor
@RestController
public class ProductController {

  private final ProductFacade facade;
  private final ProductInfoService service;

  /**
   * Find product info response.
   *
   * @param name the name
   * @return the product info response
   */
  @GetMapping("/find")
  public ProductInfoResponse find(@RequestParam String name) {
    log.info("REST request to find product info by name: {}", name);
    return service.findByName(name);
  }

  /**
   * Add product info response.
   *
   * @param request the request
   * @return the product info response
   */
  @PutMapping("/add")
  public ProductInfoResponse add(@RequestBody ProductInfoRequest request) {
    log.info("REST request to add product info: {}", request);
    return facade.addProduct(request);
  }

  /**
   * Reserve purchase.
   *
   * @param purchaseRequests the purchase requests
   * @param clientId         the client id
   * @return the purchase response wrapper
   */
  @PostMapping("/purchase/reserve")
  public PurchaseResponseWrapper reservePurchase(
      @RequestBody List<PurchaseRequest> purchaseRequests,
      @RequestParam UUID clientId
  ) {
    log.info("REST request to reserve products to purchase: {}", purchaseRequests);
    return facade.reservePurchase(purchaseRequests, clientId);
  }

  /**
   * Rollback purchase.
   *
   * @param reservationId the reservation id
   */
  @PostMapping("/purchase/rollback")
  public void rollbackPurchase(@RequestParam UUID reservationId) {
    log.info("REST request to rollback purchase, reservationId: {}", reservationId);
    facade.rollbackPurchase(reservationId);
  }

  /**
   * Commit purchase.
   *
   * @param reservationId the reservation id
   */
  @PostMapping("/purchase/commit")
  public void commitPurchase(@RequestParam UUID reservationId) {
    log.info("REST request to commit purchase, reservationId: {}", reservationId);
    facade.commitPurchase(reservationId);
  }

  /**
   * Replenish product info response.
   *
   * @param productName the product name
   * @param quantity    the quantity
   * @param sellerId    the seller id
   * @return the product info response
   */
  @PostMapping("/replenish")
  public ProductInfoResponse replenish(
      @RequestParam @NotBlank String productName,
      @RequestParam @Positive Integer quantity,
      @RequestParam UUID sellerId
  ) {
    log.info("REST request to replenish product. name: {}, quantity: {}", productName, quantity);
    return facade.replenishProduct(productName, quantity, sellerId);
  }

  /**
   * Change price product info response.
   *
   * @param productName the product name
   * @param price       the price
   * @param sellerId    the seller id
   * @return the product info response
   */
  @PostMapping("/change/price")
  public ProductInfoResponse changePrice(
      @RequestParam @NotBlank String productName,
      @RequestParam @Positive Integer price,
      @RequestParam UUID sellerId
  ) {
    log.info("REST request to change product price. name: {}, price: {}", productName, price);
    return facade.changePrice(productName, price, sellerId);
  }

  /**
   * Change discount product info response.
   *
   * @param productName the product name
   * @param discount    the discount
   * @param sellerId    the seller id
   * @return the product info response
   */
  @PostMapping("/change/discount")
  public ProductInfoResponse changeDiscount(
      @RequestParam @NotBlank String productName,
      @RequestParam @Positive Integer discount,
      @RequestParam UUID sellerId
  ) {
    log.info("REST request to change product discount. name: {}, price: {}", productName, discount);
    return facade.changeDiscount(productName, discount, sellerId);
  }

  /**
   * Filter list.
   *
   * @param name            the name
   * @param costFrom        the cost from
   * @param costTo          the cost to
   * @param discountFrom    the discount from
   * @param discountTo      the discount to
   * @param clothesCategory the clothes category
   * @param seasonCategory  the season category
   * @param sellerId        the seller id
   * @return the list
   */
  @GetMapping("/filter")
  public List<ProductInfoResponse> filter(
      @RequestParam(required = false) String name,
      @RequestParam(required = false) @PositiveOrZero Integer costFrom,
      @RequestParam(required = false) @Positive Integer costTo,
      @RequestParam(required = false) @Range(max = 100) Integer discountFrom,
      @RequestParam(required = false) @Range(max = 100) Integer discountTo,
      @RequestParam(required = false) ClothesCategory clothesCategory,
      @RequestParam(required = false) SeasonCategory seasonCategory,
      @RequestParam(required = false) UUID sellerId) {

    FilterRequest request = FilterRequest.builder()
        .name(name)
        .costFrom(costFrom)
        .costTo(costTo)
        .discountFrom(discountFrom)
        .discountTo(discountTo)
        .clothesCategory(clothesCategory)
        .seasonCategory(seasonCategory)
        .sellerId(sellerId)
        .build();

    log.info("REST request to find products by filter: {}", FilterMapGenerator
        .generateFilterMap(request));
    return service.filter(request);
  }
}
