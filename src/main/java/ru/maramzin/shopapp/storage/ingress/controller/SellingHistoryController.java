package ru.maramzin.shopapp.storage.ingress.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.storage.business.facade.ProductFacade;
import ru.maramzin.shopapp.storage.data.dto.history.selling.SellingHistoryResponse;

/**
 * The type Selling controller.
 */
@RequestMapping("histories/selling")
@RequiredArgsConstructor
@RestController
@Slf4j
public class SellingHistoryController {

  private final ProductFacade facade;

  /**
   * Find list.
   *
   * @param productName the product name
   * @return the list
   */
  @GetMapping("/find")
  public List<SellingHistoryResponse> find(@RequestParam String productName) {
    log.info("REST request to find selling histories by product name: {}", productName);
    return facade.findSellingHistoriesByProductName(productName);
  }
}
